
debug:
	rm -f test.epub
	/Applications/calibre.app/Contents/MacOS/ebook-convert test.recipe test_out.epub --test -vv --debug-pipeline debug


build:
	rm -f test.epub
	/Applications/calibre.app/Contents/MacOS/ebook-convert test.recipe test_out.epub

